﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sol_Ajax_Addition
{
    public partial class Solution : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblResult.Text = this.AddMethod();
        }

        #region  //logic for addition operation

        private string AddMethod()
        {
            var stringVal1 = txtVal1.Text;  //bind the value from textbox1
            var intVal1 = Convert.ToInt32(stringVal1);  //convert to integer

            var stringVal2 = txtVal2.Text;   //bind the value from textbox2
            var intVal2 = Convert.ToInt32(stringVal2);  //convert to integer

            var result = (intVal1 + intVal2).ToString();   //funcion for addition and convert into string

            return result;

        }

        #endregion
    }
}