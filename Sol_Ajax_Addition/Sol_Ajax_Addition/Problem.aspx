﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Problem.aspx.cs" Inherits="Sol_Ajax_Addition.Problem" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>

        <table>

            <tr>
                <td>
                    <asp:TextBox ID="txtVal1" runat="server" placeholder="Value1"></asp:TextBox>
                </td>                
            </tr>

            <tr>
                <td>
                    <asp:TextBox ID="txtVal2" runat="server" placeholder="Value2"></asp:TextBox>
                </td>                
            </tr>

            <tr>
                <td>
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="lblResult" runat="server"></asp:Label>
                </td>
            </tr>


        </table>
    
    </div>
    </form>
</body>
</html>
